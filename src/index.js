import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './components/App'

import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import reduxThunk from 'redux-thunk'

import reduces from './reduces/index'

const store = createStore(
	reduces, // Reducers
	{}, // Estado inicial
	applyMiddleware(reduxThunk)
);

ReactDOM.render(
	// todo lo que este dentro de store la app tendra acceso a ella
	<Provider store={ store }> 
		<App />
	</Provider>,
	document.getElementById('root')
);
