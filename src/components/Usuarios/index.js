import React, { Component } from 'react'
// import axios from 'axios'
import { connect } from 'react-redux'
import * as usersActions from '../../actions/usersActions'


class Usuarios extends Component {

	componentDidMount() {
		this.props.getAll()		
	}

	ponerFilas = () => this.props.users.map((usuario) => (
		<tr key={usuario.id}>
			<td>
				{usuario.name}
			</td>
			<td>
				{usuario.email}
			</td>
			<td>
				{usuario.website}
			</td>
		</tr>
	));

	render() {
		console.log(this.props)
		return (
			<div>
				<table className="tabla">
					<thead>
						<tr>
							<th>
								Nombre
							</th>
							<th>
								Correo
							</th>
							<th>
								Enlace
							</th>
						</tr>
					</thead>
					<tbody>
						{this.ponerFilas()}
					</tbody>
				</table>
			</div>
		)
	}
}

// mapear el estato al props, de todos lo reduces saco el que ne cesito y se lo paro al componetne por el connnet
const mapStateToProps = (reducers) => {
	return reducers.userReduce
}

export default connect(mapStateToProps, usersActions)(Usuarios)
// primer parametro que el connect recibe, todos los reduces que le prvedor entrega al usuario
// Segundo paramtro: actions